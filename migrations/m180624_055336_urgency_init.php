<?php

use yii\db\Migration;

/**
 * Class m180624_055336_urgency_init
 */
class m180624_055336_urgency_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('urgency', [
            'name' => 'critical',
        ]);

        $this->insert('urgency', [
            'name' => 'normal',
        ]);

        $this->insert('urgency', [
            'name' => 'low',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_055336_urgency_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_055336_urgency_init cannot be reverted.\n";

        return false;
    }
    */
}
