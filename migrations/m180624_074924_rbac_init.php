<?php

use yii\db\Migration;

/**
 * Class m180624_074924_rbac_init
 */
class m180624_074924_rbac_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $auth = Yii::$app->authManager;
            
            // add "author" role and give this role the "createPost" permission
            $manager = $auth->createRole('manager');
            $auth->add($manager);
    
            $employee = $auth->createRole('employee');
            $auth->add($employee);
    
            
            $auth->addChild($manager, $employee);
            
            $updateUsers = $auth->createPermission('updateUsers');
            $auth->add($updateUsers);
    
            $createTask = $auth->createPermission('createTask');
            $auth->add($createTask);

            $viewUser = $auth->createPermission('viewUser');
            $auth->add($viewUser);

            $indexUser = $auth->createPermission('indexUser');
            $auth->add($indexUser);

            $deleteUser = $auth->createPermission('deleteUser');
            $auth->add($deleteUser);

            $updateDeleteTask = $auth->createPermission('updateDeleteTask');
            $auth->add($updateDeleteTask); 

            $viewTask = $auth->createPermission('viewTask');
            $auth->add($viewTask);

            $indexTask = $auth->createPermission('indexTask');
            $auth->add($indexTask);
            
            $updateOwnUser = $auth->createPermission('updateOwnUser');
    
            $rule = new \app\rbac\EmployeeRule;
            $auth->add($rule);
            
            $updateOwnUser->ruleName = $rule->name;                 
            $auth->add($updateOwnUser);                                                    
            
            $auth->addChild($manager, $updateUsers);
            $auth->addChild($manager, $updateDeleteTask);
            $auth->addChild($employee, $createTask);
            $auth->addChild($employee, $viewTask);
            $auth->addChild($employee, $indexTask);
             $auth->addChild($employee, $indexUser);
            $auth->addChild($employee, $viewUser);
            $auth->addChild($employee, $updateOwnUser); 
            $auth->addChild($updateOwnUser, $updateUsers);     
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_074924_rbac_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_074924_rbac_init cannot be reverted.\n";

        return false;
    }
    */
}
